package br.com.k21;

public class CalculadoraComissao {

	// 1, 2, n
	public double Calcular(double valorVenda) {
		if (valorVenda > 500)
			//truncando em 2 casas
			return Math.floor(valorVenda * 0.05 * 100)/100;
		
		return 0;
	}

}
