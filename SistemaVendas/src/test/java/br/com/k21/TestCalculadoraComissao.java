package br.com.k21;

import org.junit.Assert;
import org.junit.Test;

public class TestCalculadoraComissao {

// AAA
	// Arrange
	// Act
	// Assert

	// PADR�ES QUE SE APLICAM A C�DIGO DE NEG�CIO, NAO NECESSARIAMENTE SE APLICAM A C�DIGO
	// DE TESTES.
	//
	// Em testes mais simples, redud�ncia n�o � um problema
	// Nomes grandes, tamb�m nao, contato que eles expressem bem o que o teste faz
	
	@Test
	public void teste_venda_de_20000_reais_retorna_1000()
	{
		double valorVenda = 20000;
		double comissaoEsperada = 1000;
		
		double comissaoRetornada = new CalculadoraComissao().Calcular(valorVenda);
		
		Assert.assertEquals(comissaoEsperada, comissaoRetornada, 0);
		
	}
	
	@Test
	public void teste_venda_de_10000_reais_retorna_500()
	{
		double valorVenda = 10000;
		double comissaoEsperada = 500;
		
		double comissaoRetornada = new CalculadoraComissao().Calcular(valorVenda);
		
		Assert.assertEquals(comissaoEsperada, comissaoRetornada, 0);
		
	}
	
	@Test
	public void teste_venda_de_10000_50_reais_retorna_500_02()
	{
		double valorVenda = 10000.50;
		double comissaoEsperada = 500.02;
		
		double comissaoRetornada = new CalculadoraComissao().Calcular(valorVenda);
		
		Assert.assertEquals(comissaoEsperada, comissaoRetornada, 0);
		
	}
	
}